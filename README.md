# SOCAC

SOCAC (SOC as code) is our final year engineering school project. It allows the deployment of SOC tools (SIEM and SOAR) on kubernetes. 
In its current version it allows the automation from A to Z of the deployment of wazuh and Nginx ingress. 
Currently the deployment of SOAR is still under development. 

## Variable file

fill "" with data and put this file to ```ansible/inventory/host_vars/all.yml``` 

```yaml
credentials: 
  wazuh_dashboard_username: ""
  wazuh_dashboard_password: ""
  wazuh_authd_pass: ""
  wazuh_api_username: ""
  wazuh_api_password: ""
  wazuh_indexer_username: ""
  wazuh_indexer_password: ""
  wazuh_cluster_key: ""


domain_list:
  wazuh: wazuh.example.fr
  thehive: thehive.example.fr

cert_list:
  - file_name: default.crt
    cm_name: cert-crt
    data: |
      -----BEGIN CERTIFICATE----- 
           fill content of cert here
           -----END CERTIFICATE-----
    
  - file_name: default.key
    cm_name: cert-key
    data: |
      -----BEGIN PRIVATE KEY-----
           fill content of key here
           -----END PRIVATE KEY-----
```

## Using SOCAC

Ensure that you have ansible installed with the latest version and kubernetes module for ansible. 
You must have your kubeconfig set at this path ```~/.kube/config```
Once you have filled variable file run the following commands : 

```bash
cd ansible/
ansible-playbook playbook/deploy_infra.yml
```


## Contributing
You can contribute by making merge request to add feature 

## License 
[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

This work is made available under the terms of the [Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).
